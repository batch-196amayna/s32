let http = require("http");

//mock collection of courses:
let courses = [
	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn ReactJS",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}
];

http.createServer(function(request, response){
	
	console.log(request.url); //differentiate request via endpoints
	console.log(request.method); //differentiate request with http method
	/*
		HTTP Requests are differentiated not only via endpoints but also with their methods
		HTTP Methods simply tells the server what action it must take or what kind of response is needed for the request
		With an HTTP methods we can actually create routes with the ame endpoint but with different methods
	*/

	if(request.url === "/" && request.method === "GET"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. GET method request") //browser is limited to GET method
	} else if (request.url === "/" && request.method === "POST"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. POST method request")
	} else if (request.url === "/" && request.method === "PUT"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. PUT method request")
	} else if (request.url === "/" && request.method === "DELETE"){
		response.writeHead(200,{'Content-Type':'text/plain'});
		response.end("This is the / endpoint. DELETE method request")
	} else if (request.url === "/courses" && request.method === "GET"){
		//'application/json' - postman will received in JSON format
		response.writeHead(200,{'Content-Type':'application/json'});//'text/plain' will results to simple one liner text and not in JSON format
		//stringify the json to send it as a response
		response.end(JSON.stringify(courses));
	} else if (request.url === "/courses" && request.method === "POST"){
		//Route to add a new course, we have to receive an input form the client
		//To be able to receive the request body or input from the request, we have to add a way to receive that input
		//In NodeJS, this is done in 2 steps:
		//requestBody will be a placeholder to contain the data (request body) passed from the client
		let requestBody = "";


		//1st step in receiving data from the request in NodeJS is called the data step.
		//data steps - will read the incoming stream of data from the client and process it so we can save it in the requestBody variable
		//1. capture the data from the request
		request.on('data', function(data){
			//console.log(data);//stream of dta from the client
			requestBody += data; // data stream is saved into the variable as a string
		}) //we get the data from our request


		

		//2nd/end step - this will rin once or after the request data has been completely sent from the client
		//2. after completely received the data from the client and thus requestBody now contains are our input
		request.on('end',function(){

			//after completely received the data from the client and thus requestBody now contains are our input
			//console.log(requestBody);

			//Initially, requestBody is in JSON format. We cannot add this our courses array beause its a string. So, we have to update requestBody variable with a parse version of the received JSON format data
			requestBody = JSON.parse(requestBody);
			//console.log(requestBody); //requestBody in now an object(terminal)

			//console.log(requestBody); //request has been completed and ready to display
			
			//add the requestBody in the array
			courses.push(requestBody);			

			//check the courses array if we were able to add our requestBody
			//console.log(courses);


			response.writeHead(200,{'Content-Type':'application/json'});
			//send the updated courses array to the client as JSON format
			response.end(JSON.stringify(courses));
		})


		
	}
	
}).listen(4000);

console.log("Server is running in localHost:4000!");